// Collect and export all routes

module.exports = function (app) {
    // Routes for NLP services
    app.use(process.env.API_ROUTE + process.env.API_ROUTE_NLP_STEM, require('./api/nlp'))

    // Route for the unsolicited speak API service
    app.use(process.env.API_ROUTE + process.env.API_ROUTE_SPEAK, require('./api/speak'))

    // Route for the optional token request service
    app.use(process.env.API_ROUTE + process.env.API_ROUTE_TOKEN, require('./api/token'))

    // Route for the optional parrot service
    app.use(process.env.API_ROUTE + process.env.API_ROUTE_PARROT, require('./api/parrot'))

    // Route for utilities including ping
    app.use('/', require('./utils'))
}
